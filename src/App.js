import React from 'react';
import NavBar from './Components/NavBar';
import Home from './Pages/Home';
import Projects from './Pages/Projects';
import Contact from './Pages/Contact';
import Error from './Pages/Error';
//import Container from 'react-bootstrap/Container'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from './Components/Layout';
import Jumbotron from './Components/Jumbotron';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';



function App() {
  return (
    <>
    <NavBar />
    <Jumbotron />
      <Layout>
        <Router>      
          
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/contact" component={Contact} />
            <Route path="/projects" component={Projects} />
          <Route component={Error} />
          </Switch>
        </Router>
      </Layout>
    	
    </>
  );
}

export default App;
