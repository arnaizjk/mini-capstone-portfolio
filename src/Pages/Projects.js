import React from 'react';
import { Card } from 'react-bootstrap';

export default function Contact () {
	return(
		<div>
			<h1>Here are Some of My Projects</h1>
			
				<Card className="cardProject">
					<Card.Img variant="top" src="holder.js/100px180?text=Image cap" />
					<Card.Body>
						<Card.Title>First Porfolio</Card.Title>
							<Card.Text>
								This is my ‘First Porfolio’ project. It was created on the 1st month of our Bootcamp. Its purpose is to showcase our Front-End Developing skills. 
							</Card.Text>
					</Card.Body>
				</Card>
			
			
				<Card className="cardProject">
					<Card.Body>
						<Card.Title>Course Booking</Card.Title>
							<Card.Text>
								This is my ‘Course Booking’ project. It was created on the 2nd month of our Bootcamp. Its purpose is to showcase our Front-End & Back-End Developing skills.
							</Card.Text>
					</Card.Body>
				</Card>
			
			
				<Card className="cardProject">
					<Card.Body>
						<Card.Title>Activities</Card.Title>
							<Card.Text>
								These are some of our JavaScript activities. It was created during our Bootcamp. Its purpose is to showcase our JavaScript and DOM Manipulation skills.
							</Card.Text>
					</Card.Body>
				</Card>
				
			
		</div>
		)
}