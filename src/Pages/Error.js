import React from 'react'

export default function Error() {	
	return (
		<div>
			<h1>404 - Page Not Found</h1>
			<h3>The page you are looking for cannot be found</h3>
			<p href="/">Back to Home Page</p>
		</div>
		)
} 