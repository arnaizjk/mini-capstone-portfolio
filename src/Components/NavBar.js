import React from 'react';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import styled from 'styled-components'


const Styles = styled.div`
	.navbar-brand, .navbar-toggle, .navbar-collapse, .navbar-link {
		color: black;

		&:hover {
			color: purple;
		}
	}


`

export default function NavBar () {
	

	return(
		<Styles>
			<Navbar bg="light" expand="lg">			
			<Navbar.Brand href="/">Jan Kristin Arnaiz</Navbar.Brand>		

			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">	
									
                    <Nav.Link href="/Projects">Projects</Nav.Link>
                    <Nav.Link href="/Contact">Contact Me</Nav.Link>
				
				</Nav>
			</Navbar.Collapse>
			</Navbar>
		</Styles>
		)
	
	
}
