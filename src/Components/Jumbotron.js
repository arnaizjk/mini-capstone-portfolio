import React from 'react'
import { Jumbotron as Jumbo, Container } from 'react-bootstrap'
import styled from 'styled-components'
import jumboBg from '../jumbo_bg.jpeg'

const Styles = styled.div`
	.jumbo {
		background: url(${jumboBg}) no-repeat fixed bottom;
		background-size: color;
		color: #efefef;
		height: 200px;
		position: relative;
		z-index: -2;
	}

	.overlay {
		background-color: #000;
		opacity: 0;
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		z-index: -1;
	}

`;

export default function Jumbotron(){
	return(
		<Styles>
			<Jumbo fluid className="jumbo">
				<div className="overlay"> </div>
				<Container>
					<h3>Welcome To My Portfolio!</h3>
					<h1>I'm Kristin Arnaiz</h1>
					<h3>Web Developer</h3>
				</Container>
			</Jumbo>
		</Styles>
		)
}


					
